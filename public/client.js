(function(){
    
    var MyApp = angular.module("MyApp",[]);
    
    var MyCtrl = function(){
        var myCtrl = this;
        myCtrl.init = function(){
            myCtrl.name = "";
            myCtrl.email = "";
            myCtrl.phone = "";
            myCtrl.comment = "";
            myCtrl.exp = "Neutral";
            myCtrl
        } 
        myCtrl.init();



        myCtrl.processForm = function(){
            for(var i in myCtrl){
                if(typeof(myCtrl[i])=="string"){
                    console.log("Prop %s: %s",i,myCtrl[i]);
                }
            }
        };
        myCtrl.resetForm = function(){
            myCtrl.init();
        }

        //form - feedbackForm, fn = email
        myCtrl.isValid = function(form,fn){
            //cannot use form.fn since fn is the name
            return (form[fn].$invalid && form[fn].$dirty);
        }
    };
    
    MyApp.controller("MyCtrl",MyCtrl);
})();
//Load express
var express = require('express');

//Create express app
var app = express();

//Static routing
app.use(express.static(__dirname+"/public"));
app.use(express.static(__dirname+"/bower_components"));

var port = process.argv[2]||3000;
app.listen(port,function(){
    console.log("App running on port "+port);
})